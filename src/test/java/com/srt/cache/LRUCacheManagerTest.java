package com.srt.cache;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LRUCacheManagerTest {

    @Test
    public void testGet_nonExistEntry_factoryNotSet_returnNull() {
        LRUCacheManager cacheManager = new LRUCacheManager(null);
        assertNull(cacheManager.get("id"));
    }

    @Test
    public void testGet_expiredEntry_factoryNotSet_removeEntryAndReturnNull() {
        Cacheable cacheable = mock(Cacheable.class);
        when(cacheable.getId()).thenReturn("id");
        when(cacheable.isExpired()).thenReturn(true);

        LRUCacheManager cacheManager = new LRUCacheManager(null);
        cacheManager.put(cacheable);
        assertEquals(1, cacheManager.getSize());
        assertNull(cacheManager.get("id"));
        assertEquals(0, cacheManager.getSize());
    }

    @Test
    public void testGet_unexpiredEntry_factoryNotSet_returnObject() {
        Cacheable cacheable = mock(Cacheable.class);
        when(cacheable.getId()).thenReturn("id");
        when(cacheable.isExpired()).thenReturn(false);

        LRUCacheManager cacheManager = new LRUCacheManager(null);
        cacheManager.put(cacheable);
        assertEquals(1, cacheManager.getSize());
        assertEquals(cacheManager.get("id"), cacheable);
        assertEquals(1, cacheManager.getSize());
    }

    @Test
    public void testGet_nonExistEntry_factorySet_returnNewObjectCreatedByFactory() {
        CacheableFactory cacheableFactory = mock(CacheableFactory.class);
        Cacheable cacheable = mock(Cacheable.class);
        when(cacheableFactory.createCacheable("id")).thenReturn(cacheable);

        LRUCacheManager cacheManager = new LRUCacheManager(cacheableFactory);
        assertEquals(cacheManager.get("id"), cacheable);
    }

    @Test
    public void testGet_expiredEntry_factorySet_returnNewObjectCreatedByFactory() {
        CacheableFactory cacheableFactory = mock(CacheableFactory.class);
        Cacheable newCacheable = mock(Cacheable.class);
        when(cacheableFactory.createCacheable("id")).thenReturn(newCacheable);
        Cacheable existingCacheable = mock(Cacheable.class);
        when(existingCacheable.getId()).thenReturn("id");
        when(existingCacheable.isExpired()).thenReturn(true);

        LRUCacheManager cacheManager = new LRUCacheManager(cacheableFactory);
        cacheManager.put(existingCacheable);
        assertEquals(1, cacheManager.getSize());
        Cacheable returnObject = cacheManager.get("id");
        assertEquals(1, cacheManager.getSize());
        assertEquals(returnObject, newCacheable);
        assertNotSame(returnObject, existingCacheable);
    }

    @Test
    public void testGet_unexpiredEntry_factorySet_returnObject() {
        Cacheable cacheable = mock(Cacheable.class);
        when(cacheable.getId()).thenReturn("id");
        when(cacheable.isExpired()).thenReturn(false);

        LRUCacheManager cacheManager = new LRUCacheManager(null);
        cacheManager.put(cacheable);
        assertEquals(1, cacheManager.getSize());
        assertEquals(cacheManager.get("id"), cacheable);
        assertEquals(1, cacheManager.getSize());
    }

    @Test
    public void testPut_nonExistKey_add() {
        LRUCacheManager cacheManager = new LRUCacheManager(null);
        assertEquals(cacheManager.getSize(), 0);
        Cacheable cacheable1 = mock(Cacheable.class);
        when(cacheable1.getId()).thenReturn("id1");
        cacheManager.put(cacheable1);
        assertEquals(cacheManager.getSize(), 1);
        assertEquals(cacheManager.get("id1"), cacheable1);

        Cacheable cacheable2 = mock(Cacheable.class);
        when(cacheable2.getId()).thenReturn("id2");
        cacheManager.put(cacheable2);
        assertEquals(cacheManager.getSize(), 2);
        assertEquals(cacheManager.get("id1"), cacheable1);
        assertEquals(cacheManager.get("id2"), cacheable2);
    }

    @Test
    public void testPut_existKey_overwriteExistingOne() {
        LRUCacheManager cacheManager = new LRUCacheManager(null);
        Cacheable existing = mock(Cacheable.class);
        when(existing.getId()).thenReturn("id");
        cacheManager.put(existing);
        assertEquals(cacheManager.get("id"), existing);
        assertEquals(cacheManager.getSize(), 1);

        Cacheable newCacheable = mock(Cacheable.class);
        when(newCacheable.getId()).thenReturn("id");
        cacheManager.put(newCacheable);
        assertEquals(cacheManager.get("id"), newCacheable);
        assertEquals(cacheManager.getSize(), 1);
    }

    @Test
    public void testCleanerThread_removesExpiredItems() throws InterruptedException {
        LRUCacheManager cacheManager = new LRUCacheManager(null);
        cacheManager.setCleanupPeriod(1000);

        Cacheable expired = mock(Cacheable.class);
        when(expired.isExpired()).thenReturn(true);
        when(expired.getId()).thenReturn("expired");
        Cacheable unexpired = mock(Cacheable.class);
        when(unexpired.isExpired()).thenReturn(false);
        when(unexpired.getId()).thenReturn("unexpired");

        cacheManager.put(expired);
        cacheManager.put(unexpired);

        assertEquals(cacheManager.getSize(), 2);
        TimeUnit.MILLISECONDS.sleep(1500);
        assertEquals(cacheManager.getSize(), 1);
        assertNull(cacheManager.get("expired"));
        assertEquals(cacheManager.get("unexpired"), unexpired);
    }
}
