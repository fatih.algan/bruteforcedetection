package com.srt.bruteforce;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UnexpiredRequestHistoryTest {
    private UnexpiredRequestHistory history;

    @Before
    public void before() {
        history = new UnexpiredRequestHistory("ip", 3);
        history.setTimeOutDuration(1000);
    }

    @Test
    public void testIsExpired_isEmpty_returnTrue() {
        assertTrue(history.isExpired());
    }

    @Test
    public void testIsExpired_lastRequestExpired_returnTrue() throws InterruptedException {
        history.addNewRequest(System.currentTimeMillis());
        assertFalse(history.isExpired());
        TimeUnit.MILLISECONDS.sleep(500);
        assertFalse(history.isExpired());
        TimeUnit.MILLISECONDS.sleep(500);
        assertTrue(history.isExpired());
    }

    @Test
    public void testIsExpired_OnceReachesTheLimit_returnAlwaysFalse() throws InterruptedException {
        history.addNewRequest(System.currentTimeMillis());
        history.addNewRequest(System.currentTimeMillis());
        history.addNewRequest(System.currentTimeMillis());
        assertFalse(history.isExpired());
        TimeUnit.SECONDS.sleep(1);
        assertFalse(history.isExpired());
        TimeUnit.SECONDS.sleep(1);
        assertFalse(history.isExpired());
    }

    @Test
    public void testAddNewRequest_isFull_returnTrue() {
        assertFalse(history.addNewRequest(System.currentTimeMillis()));
        assertFalse(history.addNewRequest(System.currentTimeMillis()));
        assertTrue(history.addNewRequest(System.currentTimeMillis()));
    }

    @Test
    public void testAddNewRequest_removeExpiredRequests() throws InterruptedException {
        assertFalse(history.addNewRequest(System.currentTimeMillis()));
        assertFalse(history.addNewRequest(System.currentTimeMillis()));
        TimeUnit.SECONDS.sleep(1);
        //first two request is expired, it will be removed
        assertFalse(history.addNewRequest(System.currentTimeMillis()));
        assertFalse(history.addNewRequest(System.currentTimeMillis()));
        assertTrue(history.addNewRequest(System.currentTimeMillis()));
    }

    @Test
    public void testIsNeverExpire_OnceHistoryFull_returnAlwaysTrue() throws InterruptedException {
        assertFalse(history.isNeverExpire());
        history.addNewRequest(System.currentTimeMillis());
        history.addNewRequest(System.currentTimeMillis());
        assertFalse(history.isNeverExpire());
        history.addNewRequest(System.currentTimeMillis());
        assertTrue(history.isNeverExpire());

        TimeUnit.SECONDS.sleep(1);
        history.addNewRequest(System.currentTimeMillis());
        history.addNewRequest(System.currentTimeMillis());
        assertTrue(history.isNeverExpire());
    }

    @Test
    public void testSetTimeOut_setTo0_allRequestsExpired() {
        history.addNewRequest(System.currentTimeMillis());
        history.addNewRequest(System.currentTimeMillis());
        assertFalse(history.isExpired());
        history.setTimeOutDuration(0);
        assertTrue(history.isExpired());
    }

}
