package com.srt.bruteforce;

import com.srt.cache.LRUCacheManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BruteForceDetectionImplTest {
    private BruteForceDetection bruteForceDetection;
    private LRUCacheManager<UnexpiredRequestHistory> cacheManager;

    @Before
    public void before() {
        cacheManager = mock(LRUCacheManager.class);
        bruteForceDetection = new BruteForceDetectionImpl(2, cacheManager);
    }

    @Test
    public void testIsBruteForceAttack_noHistory_returnFalse() {
        when(cacheManager.get("ip")).thenReturn(null);
        assertFalse(bruteForceDetection.isBruteForceAttack("ip"));
    }

    @Test
    public void testIsBruteForceAttack_neverExpireHistory_returnTrue() {
        UnexpiredRequestHistory history = mock(UnexpiredRequestHistory.class);
        when(cacheManager.get("ip")).thenReturn(history);
        when(history.isNeverExpire()).thenReturn(true);
        assertTrue(bruteForceDetection.isBruteForceAttack("ip"));
    }

    @Test
    public void testIsBruteForceAttack_nonNeverExpireHistory_historyIsNotFull_returnFalse() {
        UnexpiredRequestHistory history = mock(UnexpiredRequestHistory.class);
        when(cacheManager.get("ip")).thenReturn(history);
        when(history.isNeverExpire()).thenReturn(false);
        when(history.addNewRequest(System.currentTimeMillis())).thenReturn(false);
        assertFalse(bruteForceDetection.isBruteForceAttack("ip"));
    }

    @Test
    public void testIsBruteForceAttack_nonNeverExpireHistory_historyIsFull_returnTrue() {
        UnexpiredRequestHistory history = mock(UnexpiredRequestHistory.class);
        when(cacheManager.get("ip")).thenReturn(history);
        when(history.isNeverExpire()).thenReturn(false);
        when(history.addNewRequest(anyLong())).thenReturn(true);
        assertTrue(bruteForceDetection.isBruteForceAttack("ip"));
    }

}
