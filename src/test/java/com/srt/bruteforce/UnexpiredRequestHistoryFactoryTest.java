package com.srt.bruteforce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UnexpiredRequestHistoryFactoryTest {
    private UnexpiredRequestHistoryFactory unexpiredRequestHistoryFactory;

    @Before
    public void before() {
        unexpiredRequestHistoryFactory = new UnexpiredRequestHistoryFactory(45);
    }

    @Test
    public void testCreateCacheable_invalidParameter_returnNull() {
        assertNull(unexpiredRequestHistoryFactory.createCacheable(null));
        assertNull(unexpiredRequestHistoryFactory.createCacheable(14));
    }

    @Test
    public void testCreateCacheable_validParameter() {
        UnexpiredRequestHistory unexpiredRequestHistory = unexpiredRequestHistoryFactory.createCacheable("ip");
        assertEquals(unexpiredRequestHistory.getId(), "ip");
    }
}
