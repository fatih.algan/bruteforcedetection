package com.srt.bruteforce.scenario;

import com.srt.bruteforce.BruteForceDetectionImpl;
import com.srt.bruteforce.UnexpiredRequestHistory;
import com.srt.bruteforce.UnexpiredRequestHistoryFactory;
import com.srt.cache.CacheableFactory;
import com.srt.cache.LRUCacheManager;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class BruteForceDetectionImplScenarioTest {

    @Test
    public void testConcurrentThreads() throws InterruptedException {
        //setup brute force detector
        int limit = 10;
        CacheableFactory<UnexpiredRequestHistory> requestHistoryFactory = new UnexpiredRequestHistoryFactory(limit);
        LRUCacheManager<UnexpiredRequestHistory> cacheManager = new LRUCacheManager<UnexpiredRequestHistory>(requestHistoryFactory);
        int cleanupPeriod = 2000;
        cacheManager.setCleanupPeriod(cleanupPeriod);
        BruteForceDetectionImpl bruteForceDetection = new BruteForceDetectionImpl(limit, cacheManager);

        TestLoginController loginController = new TestLoginController(bruteForceDetection);
        List<TestRequesterThread> requesterThreadList = new ArrayList<TestRequesterThread>();
        int threadCount = 20000;
        int threadLifeTime = 10000;
        long threadDeathTime = System.currentTimeMillis() + threadLifeTime;

        //create and run login requester threads
        //all attacker threads attacks at least one time before dead
        //noon-attacker threads do not request equal or more than limit count in a second
        for (int i = 0; i < threadCount; i++) {
            // 20% threads are attacker
            boolean isBruteForceAttacker = ThreadLocalRandom.current().nextInt(1, 11) < 3;

            TestRequesterThread requesterThread = new TestRequesterThread(i + "", threadDeathTime, limit, cleanupPeriod, isBruteForceAttacker, loginController);
            requesterThreadList.add(requesterThread);
            new Thread(requesterThread).start();
        }
        Thread.sleep(threadLifeTime + 1000);


        //aseertions
        for (TestRequesterThread requesterThread : requesterThreadList) {
            Long firstBruteForceAttackAttemptTime = requesterThread.getFirstBruteForceAttackAttemptTime();
            Long lastNotAttackRequestTime = loginController.getLastNotAttackTime(requesterThread.getIp());
            Long firstAttackDetectionTime = loginController.getFirstAttackTime(requesterThread.getIp());
//            System.out.println(
//                    "ip:   " + requesterThread.getIp() +
//                            "\tisAttacker:   " + requesterThread.isBruteForceAttacker() +
//                            "\tfirstAttemptTime: \t" + requesterThread.getFirstBruteForceAttackAttemptTime() +
//                            "\tfirstAttackDetectionTime: \t" + firstAttackDetectionTime +
//                            "\tlastNotAttackRequestTime: \t" + lastNotAttackRequestTime +
//                            "\tattackRequestTime: \t" + loginController.getAttackRequestCount(requesterThread.getIp()) +
//                            "\tnotAttackRequestTime: \t" + loginController.getNotAttackRequestCount(requesterThread.getIp())
//            );
            if(requesterThread.isBruteForceAttacker()) {
                assertTrue(firstAttackDetectionTime >= firstBruteForceAttackAttemptTime);
                assertTrue(lastNotAttackRequestTime <= firstAttackDetectionTime);
            }
            else {
                assertNull(firstBruteForceAttackAttemptTime);
                assertEquals(loginController.getAttackRequestCount(requesterThread.getIp()), 0);
            }
        }
    }
}
