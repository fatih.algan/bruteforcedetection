package com.srt.bruteforce.scenario;

import com.srt.bruteforce.BruteForceDetection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Test class that simulates a request controller
 * Each login request is checked by bruteForceDetection in case of brute force attack
 * Contains test information which is used for assertion
 */
public class TestLoginController {
    private BruteForceDetection bruteForceDetection;
    private Map<String, List<Long>[]> ipRequestTimeMap = new HashMap<String, List<Long>[]>();

    public TestLoginController(BruteForceDetection bruteForceDetection) {
        this.bruteForceDetection = bruteForceDetection;
    }

    public void login(String ip) {
        boolean bruteForceAttack = bruteForceDetection.isBruteForceAttack(ip);
        synchronized (this) {
            List<Long>[] listArray = ipRequestTimeMap.get(ip);
            if(listArray == null) {
                listArray = new List[2];
                listArray[0] = new ArrayList<Long>();
                listArray[1] = new ArrayList<Long>();
                ipRequestTimeMap.put(ip, listArray);
            }
            long time = System.currentTimeMillis();
            if(bruteForceAttack) {
                listArray[0].add(time);
            }
            else {
                listArray[1].add(time);
            }
        }
    }

    public Long getFirstAttackTime(String ip) {
        List<Long> list = ipRequestTimeMap.get(ip)[0];
        return list.isEmpty() ? null : list.get(0);
    }

    public Long getLastNotAttackTime(String ip) {
        List<Long> notAttackList = ipRequestTimeMap.get(ip)[1];
        return notAttackList.isEmpty() ? null : notAttackList.get(notAttackList.size() - 1);
    }

    public int getAttackRequestCount(String ip) {
        return ipRequestTimeMap.get(ip)[0].size();
    }

    public int getNotAttackRequestCount(String ip) {
        return ipRequestTimeMap.get(ip)[1].size();
    }
}
