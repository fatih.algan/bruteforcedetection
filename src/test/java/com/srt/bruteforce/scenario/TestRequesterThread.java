package com.srt.bruteforce.scenario;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Test class that simulates requester client
 * It generates random number of login requests in random period of time
 * Contains test information which is used for assertion
 */
public class TestRequesterThread implements Runnable {
    private String ip;
    private long deathTime;
    private int limit;
    private int cleanupPeriod;
    private boolean bruteForceAttacker;
    private TestLoginController loginController;
    private Long firstBruteForceAttackAttemptTime;

    public TestRequesterThread(String ip, long deathTime, int limit, int cleanupPeriod, boolean bruteForceAttacker, TestLoginController loginController) {
        this.ip = ip;
        this.deathTime = deathTime;
        this.limit = limit;
        this.cleanupPeriod = cleanupPeriod;
        this.bruteForceAttacker = bruteForceAttacker;
        this.loginController = loginController;
    }

    @Override
    public void run() {
        long  currentTime;
        while((currentTime = System.currentTimeMillis()) < deathTime) {
            makeLoginRequest(generateRequestCount());
            try {
                long remainingTime = deathTime - currentTime;
                long minTime = Math.min(cleanupPeriod * 3 / 2, remainingTime);
                long millis = ThreadLocalRandom.current().nextLong(minTime, remainingTime + 1);
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(bruteForceAttacker && firstBruteForceAttackAttemptTime == null) {
            makeLoginRequest(limit);
        }
    }

    private void makeLoginRequest(int count) {
        long time = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            loginController.login(ip);
        }
        if(bruteForceAttacker && count >= limit && firstBruteForceAttackAttemptTime == null) {
            firstBruteForceAttackAttemptTime = time;
        }
    }

    private int generateRequestCount() {
        if(bruteForceAttacker) {
            return ThreadLocalRandom.current().nextInt(1, limit * 3 + 1);
        }
        return ThreadLocalRandom.current().nextInt(1, limit);
    }

    public Long getFirstBruteForceAttackAttemptTime() {
        return firstBruteForceAttackAttemptTime;
    }

    public String getIp() {
        return ip;
    }

    public boolean isBruteForceAttacker() {
        return bruteForceAttacker;
    }
}
