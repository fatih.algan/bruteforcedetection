package com.srt.cache;

import org.apache.log4j.Logger;

import java.util.*;

/**
 * Least Recently Used Thread-safe Timed Generic Cache Manager
 *
 * LRU: LinkedHashMap is used to make the cahe LRU
 *
 * Thread-safe: Map object is synchronized by using semaphore object lock, that makes the cache thred-safe
 *
 * Timed: Cache items are removed by using clean-up-thread which runs periodically
 * Clean-up-thread removes expired items from cache by asking cacheable objects.
 * This method provides different expiration polices encapsulated in cacheable objects
 *
 * Generic: Any class implements Cacheable interface can be used as cache item
 * Any object can be used as key for cache item
 *
 */
public class LRUCacheManager<T extends Cacheable> implements CacheManager<T> {
    private static int DEFAULT_CLEANUP_PERIOD = 5000;
    private static Logger logger = Logger.getLogger(LRUCacheManager.class);

    //LRU cache object. LinkedHashMap constructor is set to make the map access-ordered
    private LinkedHashMap<Object, T> cacheMap = new LinkedHashMap<Object, T>(2, 0.75f, true);

    //if factory is set, new object is created by using this factory,
    //if the object is not found or expired in @get method
    private CacheableFactory<T> cacheableFactory;

    private long cleanupPeriod = DEFAULT_CLEANUP_PERIOD;

    //this field is used to make the cache object synchronized
    private final Object semaphore = new Object();

    /**
     * @param cacheableFactory: to create cacheable object if non-exist
     */
    public LRUCacheManager(CacheableFactory<T> cacheableFactory) {
        this.cacheableFactory = cacheableFactory;
        startCleaner();
    }

    private void startCleaner() {
        Thread threadCleanerUpper = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        long startTime = System.currentTimeMillis();
                        int expiredItemCount = clearExpiredCacheItems();
                        long duration = System.currentTimeMillis() - startTime;
                        logger.info("Cleaned item count: " + expiredItemCount + ", duration: " + duration + " millis, itemCount: " + getSize());
                        Thread.sleep(cleanupPeriod);
                    } catch (Exception e) {
                        logger.error("Exception in Cache Cleaner: ", e);
                    }
                }
            }
        });
        threadCleanerUpper.setPriority(Thread.MIN_PRIORITY);
        threadCleanerUpper.start();
    }

    private int clearExpiredCacheItems() {
        int expiredItemCount = 0;
        synchronized (semaphore) {
            int count = 0;
            int size = cacheMap.size();
            while (!cacheMap.isEmpty() && count < size) {
                Object id = cacheMap.keySet().iterator().next();
                T cacheable = cacheMap.get(id);
                if(cacheable.isExpired()) {
                    cacheMap.remove(id);
                    expiredItemCount++;
                }
                else if(!cacheable.isNeverExpire()) {
                    break;
                }
                count++;
            }
        }
        return expiredItemCount;
    }

    @Override
    public void put(T item) {
        synchronized (semaphore) {
            cacheMap.put(item.getId(), item);
        }
    }

    /**
     * if cacheableFactory is set and item not found or expired, new object will be created by the factory
     * if cacheablefactory is not set and the object is expired, object will be removed
     * @param id: cache item key
     * @return cache item
     */
    @Override
    public T get(Object id) {
        T object;
        synchronized (semaphore) {
            object = cacheMap.get(id);
            if (cacheableFactory != null) {
                if (object == null || object.isExpired()) {
                    object = cacheableFactory.createCacheable(id);
                    cacheMap.put(id, object);
                }
            } else {
                if (object == null) {
                    return null;
                }
                if (object.isExpired()) {
                    cacheMap.remove(id);
                    return null;
                }
            }
        }
        return object;
    }

    /**
     * @param cleanupPeriod: thread cleaner re-run period in millis
     */
    public void setCleanupPeriod(int cleanupPeriod) {
        this.cleanupPeriod = cleanupPeriod;
    }

    /**
     * @return cached item count
     */
    public int getSize() {
        return cacheMap.size();
    }
}
