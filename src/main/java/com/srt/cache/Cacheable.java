package com.srt.cache;

/**
 * Timed cache object interface
 */
public interface Cacheable {
    /**
     * expiration strategy is encapsulated by cacheable object
     * @return true expired, false otherwise
     */
    public boolean isExpired();

    /**
     * @return object key for the cache item
     */
    public Object getId();

    boolean isNeverExpire();
}