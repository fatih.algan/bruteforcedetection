package com.srt.cache;

/**
 * Generic Timed cache manager interface
 */
public interface CacheManager<T extends Cacheable> {

    void put(T item);

    T get(Object id);
}
