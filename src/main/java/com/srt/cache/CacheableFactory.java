package com.srt.cache;

/**
 * Cacheable object factory
 */
public interface CacheableFactory<T extends Cacheable> {
    T createCacheable(Object id);
}
