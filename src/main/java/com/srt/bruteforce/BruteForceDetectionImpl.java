package com.srt.bruteforce;

import com.srt.cache.LRUCacheManager;

/**
 * This class detects brute force attacks with memory and performance optimized, thread-safe way
 */
public class BruteForceDetectionImpl extends BruteForceDetection {
    private LRUCacheManager<UnexpiredRequestHistory> cacheManager;

    public BruteForceDetectionImpl(int limit, LRUCacheManager<UnexpiredRequestHistory> cacheManager) {
        super(limit);
        this.cacheManager = cacheManager;
    }

    /**
     * if brute force attack from an ip is detected, it always returns true without creating new request
     * otherwise new request is added into history and returns whether brute force attack is happened or not
     *
     * @param ipAddress: ip
     * @return true if brute force attack, false otherwise
     */
    @Override
    public boolean isBruteForceAttack(String ipAddress) {
        UnexpiredRequestHistory history = cacheManager.get(ipAddress);

        //addNewRequest method returns whether unexpired history list is full or not
        //if the list is full then we know that this ip is brute force attacking
        return history != null && (history.isNeverExpire() || history.addNewRequest(System.currentTimeMillis()));
    }
}
