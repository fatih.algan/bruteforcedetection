package com.srt.bruteforce;

import com.srt.cache.Cacheable;

import java.util.LinkedList;

/**
 * Cacheable class that contains only unexpired requests
 * Expired requests are removed while adding new request
 *
 * Once its unexpired requests reaches the limit, it never expires
 */
public class UnexpiredRequestHistory implements Cacheable {
    private static final long DEFAULT_TIME_OUT_DURATION = 1000;

    private String ipAddress;
    private int limit;
    private long timeOutDuration = DEFAULT_TIME_OUT_DURATION;
    private boolean neverExpire;
    //first request is the eldest one
    private LinkedList<Long> requestTimeList = new LinkedList<Long>();

    public UnexpiredRequestHistory(String ipAddress, int limit) {
        this.ipAddress = ipAddress;
        this.limit = limit;
    }

    @Override
    public String getId() {
        return ipAddress;
    }

    /**
     * @return true: if the last request is expired returns true,  false: otherwise
     * once number of requests reaches the limit under time out period, it never expires
     */
    @Override
    public synchronized boolean isExpired() {
        return requestTimeList.isEmpty() || !neverExpire && isRequestExpired(requestTimeList.getLast());
    }

    private boolean isRequestExpired(Long requestTime) {
        return System.currentTimeMillis() - requestTime >= timeOutDuration;
    }

    /**
     * adds a new request, clears expired requests
     *
     * @param time: request time
     * @return true if unexpired request count reaches the limit
     */
    public synchronized boolean addNewRequest(Long time) {
        removeExpiredRequests();
        requestTimeList.add(time);

        if (requestTimeList.size() == limit) {
            this.neverExpire = true;
        }
        return this.neverExpire;
    }

    private void removeExpiredRequests() {
        while (!requestTimeList.isEmpty() && isRequestExpired(requestTimeList.getFirst())) {
            requestTimeList.removeFirst();
        }
    }

    /**
     *
     * @param timeOutDuration: request timeout duration in millis
     */
    public void setTimeOutDuration(long timeOutDuration) {
        this.timeOutDuration = timeOutDuration;
    }

    @Override
    public boolean isNeverExpire() {
        return neverExpire;
    }
}
