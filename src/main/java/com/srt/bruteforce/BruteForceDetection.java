package com.srt.bruteforce;

/**
 * Base class for Brute Force Detection
 */
public abstract class BruteForceDetection {
    protected int limit;

    protected BruteForceDetection(int limit) {
        this.limit = limit;
    }

    public abstract boolean isBruteForceAttack(String ipAddress);
}
