package com.srt.bruteforce;

import com.srt.cache.CacheableFactory;

public class UnexpiredRequestHistoryFactory implements CacheableFactory<UnexpiredRequestHistory> {
    private int limit;

    public UnexpiredRequestHistoryFactory(int limit) {
        this.limit = limit;
    }

    @Override
    public UnexpiredRequestHistory createCacheable(Object identifier) {
        if (identifier == null || !String.class.isInstance(identifier)) {
            return null;
        }
        return new UnexpiredRequestHistory((String) identifier, limit);
    }
}
